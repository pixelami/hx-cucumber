package tool;
class PathTool
{
	public static function camelToUnderscore(camelCase:String)
	{
		var p = 0;
		var str = "";
		var words = [];
		while(p++ < camelCase.length - 1)
		{
			var c = camelCase.charCodeAt(p);
			if(isUpperCase(c))
			{
				if(str != "") words.push(str);
				str = camelCase.charAt(p).toLowerCase();
			}
			else
			{
				str += camelCase.charAt(p);
			}
		}
		words.push(str);
		return words.join("_");
	}

	static inline function isUpperCase(c:Int):Bool
	{
		return c  > 64 && c < 91;
	}

	public static function getRelativePath(fromPath:String, toPath:String):String
	{
		var relPath = "";
		var absFromPath = sys.FileSystem.fullPath(fromPath);
		var fromParts = absFromPath.split("/");
		if(!sys.FileSystem.isDirectory(absFromPath)) fromParts.pop();
		var absToPath = sys.FileSystem.fullPath(toPath);
		var toParts = absToPath.split("/");

		while(fromParts[0] == toParts[0])
		{
			fromParts.shift();
			toParts.shift();
		}
		var d = fromParts.length - toParts.length;
		if(d > 0)
		{
			for(i in 0...(d+1)) relPath += "../";
			relPath += toParts.join("/");
		}
		else if(d < 0)
		{
			relPath += "../" + toParts.join("/");
		}
		else
		{
			relPath += toParts.join("/");
		}
		return relPath;
	}

	public static function split(path:String):{tail:String, head:String}
	{
		var idx = path.lastIndexOf("/");
		var tail = path.substring(0,idx);
		var head = path.substring(idx);
		return {tail:tail, head:head};
	}

	public static function splitExt(path:String):{name:String, ext:String}
	{
		var idx = path.lastIndexOf(".");
		var name = path.substring(0,idx);
		var ext = path.substring(idx);
		return {name:name, ext:ext};
	}
}
