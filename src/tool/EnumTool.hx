package tool;
class EnumTool
{
	public static function create(e:Enum<Dynamic>, name:String, params:Array<Dynamic>):Enum<Dynamic>
	{
		return Type.createEnum(e, name, params);
	}
}
