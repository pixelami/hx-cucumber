package cucumber;

import cucumber.StepBuilder.StepType;
import tool.EnumTool;
import tool.PathTool;
import haxe.macro.Context;
import haxe.macro.Type;
import haxe.macro.Expr;


typedef StepInfo = { stepType:StepType, name:String, method:String }
typedef MetadataEntry = { name : String, params : Array<Expr>, pos : Position }
enum StepType
{
	Given(pattern:String);
	When(pattern:String);
	Then(pattern:String);
	Before(tags:Array<String>);
	After(tags:Array<String>);
}

class StepBuilder
{

	static inline var __self:String = "__self";
	static inline var __inst:String = "__inst";
	static inline var __callback:String = "__callback";

	public static function build():Array<Field>
	{
		var builder = new StepBuilder();
		var fields:Array<Field> = Context.getBuildFields();
		builder.processFields(fields);

		fields.push(builder.buildWorldField());
		fields.push(builder.buildInstanceField());
		fields.push(builder.buildCucumberCallbackField());
		fields.push(builder.build__init__Field());
		fields.push(builder.buildInitField());

		for(field in builder.worldFunctionVars) {
			fields.push(field);
		}

		return fields;
	}

    public var steps:Array<StepInfo>;
	var pos:Position;
	var className:String;
	public var worldFunctionVars(default, null):Array<Field>;
	var relativeWorldPath:String;

	public function new()
	{
		steps = [];
		worldFunctionVars = [];
		pos = Context.currentPos();
		className = Context.getLocalClass().toString();
		processWorld();
	}

	function processWorld()
	{
		var world:String = "";
		var classType:ClassType = Context.getLocalClass().get();
		if(classType.meta.has(":World"))
		{
			var metadata:Metadata = classType.meta.get();
			for(meta in metadata) {
				if(meta.name == ":World")
				{
					for(param in meta.params)
					{

						var p = param;
						var parts = [];
						while(p != null)
						{

							var n = switch(p.expr)
							{
								case EField(e, name): parts.push(name); e;
								case EConst(c): switch(c){ case CIdent(name): parts.push(name); null; default: }
								default:
							}
							p = n;
						}
						parts.reverse();
						world = parts.join(".");
					}
				}
			}

			var worldType:haxe.macro.Type = Context.getType(world);
			var worldClass:ClassType = switch(worldType) { case TInst(t, params): t.get(); default: };



			for(field in worldClass.fields.get())
			{
				switch(field.kind)
				{
					case FMethod(k): switch(k) { case MethodKind.MethNormal: addMethodVarField(field); default: }
					default:
				}
			}

			setRelativeWorldPath(worldClass);
		}
	}

	/*
	Currently we take our World class name e.g. MyWorld and convert to my_world.js
	We also calculate the relative file path to my_world.js
	 */
	function setRelativeWorldPath(worldClass:ClassType)
	{
		var worldFile = Context.getPosInfos(worldClass.pos).file;
		var stepFile = Context.getPosInfos(Context.getLocalClass().get().pos).file;

		var relPath = PathTool.getRelativePath(stepFile, worldFile);

		var parts = PathTool.split(relPath);
		var underscore_name = PathTool.camelToUnderscore(parts.head);

		var name = PathTool.splitExt(underscore_name).name;
		relativeWorldPath = parts.tail + "/" + name + ".js";
	}

	/*
	Convert methods declared in World to function vars in the ISteps implementer.
	It's not ideal but cucumber-js insists of modifying the Steps prototype.
	So we play nice and do it the cucumber-js way which is to assume that World methods
	will be available via 'this' in the Steps definition class.
	*/
	function addMethodVarField(field:ClassField)
	{
		var expr = Context.getTypedExpr(field.expr());

		var argTypes = [];
		var retType = null;

		switch(expr.expr) {
			case EFunction(name, f):
				for(arg in f.args) { argTypes.push(arg.type); };
				retType = f.ret;
			default:
		}

		var func = TFunction(argTypes, retType);
		var f = { name : field.name, kind : FieldType.FVar(func,null), pos : pos };
		worldFunctionVars.push(f);
	}


	public function processFields(fields:Array<Field>)
	{
		for (field in fields)
		{
			for(meta in field.meta)
			{
				var t = getStepType(meta);
				if(t == null) continue;

				steps.push({
					stepType:t,
					name: meta.name.substring(1),
					method:field.name
				});
			}
		}
	}

	function getStepType(meta:MetadataEntry):StepType
	{
		if(meta.name.substr(0,1) != ":") throw "Please make sure that "+ meta.name +" is prefixed with a colon to make it compile time metadata";
		var e = null;
		if(meta.name == ":Before" || meta.name == ":After")
		{
			e = EnumTool.create(StepType, meta.name.substring(1), [getTags(meta)]);
		}
		else
		{
			var pattern = getStepPattern(meta);
			e = EnumTool.create(StepType, meta.name.substring(1), [pattern]);
		}
		return cast e;
	}

	function getStepPattern(meta:MetadataEntry):String
	{
		if(meta.params.length == 0) return null;
		return switch(meta.params[0].expr)
		{
			case EConst(c): switch(c){ case CString(s): s; default: }
			default:
		}
	}

	function getTags(meta:MetadataEntry):Array<String>
	{
		var tags = [];
		for(m in meta.params)
		{
			var t = switch(m.expr)
			{
				case EConst(c): switch(c){ case CString(s): s; default: }
				default:
			}
			if(t == null) continue;
			tags.push(t);
		}
		return tags;
	}

	public function toJS(step:StepInfo):String
	{
		var s = "";
		// The cucumber step that we are registering with
		var stepMethod = [className, __self, step.name].join(".");
		// The target method that we are registering (i.e. our Step code)
		var stepTarget = [className, __inst, step.method].join(".");

		switch(step.stepType)
		{
			case StepType.Given(pattern), StepType.When(pattern), StepType.Then(pattern):

				var pattern = StringTools.replace(pattern, "\\", "\\\\");
				s = stepMethod + "(/" + pattern + "/, " + stepTarget + ")";

			case StepType.After(tags), StepType.Before(tags):

				if(tags.length > 0)
				{
					var escapedTags = tags.join('\\",\\"');
					escapedTags = '\\"' + escapedTags + '\\"';
					s = stepMethod + "(" + escapedTags + "," + stepTarget + ")";
				}
				else
				{
					s = stepMethod + "("+ stepTarget + ")";
				}

			default:
		}

		return s;
	}


	public function buildInitField(): Field
	{
		var block:Array<Expr> = [];

		if(relativeWorldPath != null) block.push(Context.parse("untyped __js__('this.World = require(\\'"+relativeWorldPath+"\\').World')", pos));
		block.push(Context.parse(__self + " = untyped __js__('this')", pos));
		block.push(Context.parse(__inst+" = new "+className+"()", pos));

		for(step in steps)
		{
		    var stepSrc = "untyped __js__('"+toJS(step)+"')";
			//trace(stepSrc);
			block.push(Context.parse(stepSrc, pos));
		}

		var func = {
			args: [],
			expr: { expr: EBlock(block), pos:pos },
			params: [],
			ret: null
		}

		var f = {
			name:"init",
			doc: null,
			meta: [],
			access: [AStatic],
			kind: FFun(func),
			pos: pos
		}
		return f;
	}

	public function build__init__Field(): Field
	{
		var block:Array<Expr> = [];
		block.push(Context.parse("untyped __js__('module.exports = "+className+".init')", pos));


		var func = {
			args: [],
			expr: { expr: EBlock(block), pos:pos },
			params: [],
			ret: null
		}

		var f = {
			name:"__init__",
			doc: null,
			meta: [],
			access: [AStatic],
			kind: FFun(func),
			pos: pos
		}
		return f;
	}

	// TODO build wrapper that stores cucumber callback object
	public function buildStepWrapperFunction():Field
	{
		var block:Array<Expr> = [];

		var func = {
			args: [],
			expr: { expr: EBlock(block), pos:pos },
			params: [],
			ret: null
		}

		var f = {
			name:"callStep",
			doc: null,
			meta: [],
			access: [APublic],
			kind: FFun(func),
			pos: pos
		}
		return f;
	}

	public function buildWorldField():Field
	{
		var world = TPath({ pack : [], name : "Dynamic", params : [], sub : null });
		return { name : __self, doc : null, meta : [], access : [AStatic], kind : FVar(world,null), pos : pos };
	}

	public function buildInstanceField():Field
	{
		var classType:ClassType = Context.getLocalClass().get();
		var inst = TPath({ pack : classType.pack, name : classType.name, params : [], sub : null });
		return { name : __inst, doc : null, meta : [], access : [AStatic], kind : FVar(inst,null), pos : pos };
	}

	public function buildCucumberCallbackField():Field
	{
		var inst = TPath({ pack : [], name : "Dynamic", params : [], sub : null });
		return { name : __callback, doc : null, meta : [], access : [AStatic], kind : FVar(inst,null), pos : pos };
	}

}
