package cucumber;

import haxe.macro.Expr;
import haxe.macro.Context;

class WorldBuilder
{
	public static function build():Array<Field>
	{
		var builder = new WorldBuilder();
		var fields:Array<Field> = Context.getBuildFields();
		builder.processFields(fields);

		fields.push(builder.build__init__Field());

		return fields;
	}

	public var className:String;
	public var pos:Position;

	public function new()
	{
		pos = Context.currentPos();
		className = Context.getLocalClass().toString();
	}

	public function processFields(fields:Array<Field>)
	{

	}

	public function build__init__Field(): Field
	{
		var block:Array<Expr> = [];

		block.push(Context.parse("untyped __js__('var WorldConstructor = function WorldConstructor(cb){ cb("+className+".prototype); }')",pos));
		block.push(Context.parse("untyped __js__('exports.World = WorldConstructor')", pos));

		var func = {
			args: [],
			expr: { expr: EBlock(block), pos:pos },
			params: [],
			ret: null
		}

		var f = {
			name:"__init__",
			doc: null,
			meta: [],
			access: [AStatic],
			kind: FFun(func),
			pos: pos
		}
		return f;
	}
}
